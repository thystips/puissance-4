package com.antoinethys.puissance4.serveur.Json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.antoinethys.puissance4.serveur.moteur.Plateau;
import org.jetbrains.annotations.Nullable;
import com.antoinethys.puissance4.serveur.serveur.ThreadServeur;

/**
 * <b>Utilisation de Jackson</b>
 *
 * Cette classe permet de construire le message envoyé par le Thread au Client
 *
 * @version 1.0.0
 * @see ThreadServeur
 */
public @Data class ThreadJson {
    @JsonProperty("jeSuis") @Nullable
    private final Integer jeSuis;
    @JsonProperty("quiJoue") @Nullable
    private final Integer quiJoue;
    @JsonProperty("plateau") @Nullable
    private final Plateau plateau;
    @JsonProperty("message")
    private final String message;

    @JsonCreator
    public ThreadJson(
            @Nullable @JsonProperty("jeSuis") Integer jeSuis,
            @Nullable @JsonProperty("quiJoue") Integer quiJoue,
            @Nullable @JsonProperty("plateau") Plateau plateau,
            @JsonProperty("message") String message
    ) {
        this.jeSuis = jeSuis;
        this.quiJoue = quiJoue;
        this.plateau = plateau;
        this.message = message;
    }

    @Nullable
    @JsonProperty("jeSuis")
    public Integer getJeSuis() {
        return this.jeSuis;
    }

    @Nullable
    @JsonProperty("quiJoue")
    public Integer getQuiJoue() {
        return this.quiJoue;
    }

    @Nullable
    @JsonProperty("plateau")
    public Plateau getPlateau() {
        return this.plateau;
    }

    @JsonProperty("message")
    public String getMessage() {
        return this.message;
    }

}
