package com.antoinethys.puissance4.serveur.moteur;

/**
 * <b>Définition de ce qu'est une Case</b>
 * @author Antoine Thys
 * @version 1.0.0
 * @see Case
 * @see Plateau
 */
public class Case {
    /**
     * Contenu d'une Case
     * <p>Pour plus d'information regarder la documentation de l'ENUM TypeCase</p>
     * @see TypeCase
     *
     * @see Case#getValeur()
     * @see Case#setValeur(TypeCase)
     *
     * @since 1.0.0
     */
    private TypeCase m_valeur;

    /**
     * <b>Constructeur de la Class</b>
     * @see Case
     *
     * <p>
     *     Permet d'instancier m_valeur pour obtenir une case vide
     * @see Case#m_valeur
     * @see TypeCase#VIDE
     * </p>
     * @since 1.0.0
     */
    public Case() {
        this.m_valeur = TypeCase.VIDE;
    }

    /**
     * <b>Permet de retourner la valeur d'une case</b>
     * @return TypeCase
     * @see Case#m_valeur
     * @since 1.0.0
     */
    public TypeCase getValeur() {
        return this.m_valeur;
    }

    /**
     * <b>Définition de la valeur de la case</b>
     * @param valeur (TypeCase) type de la case
     * @see Case#m_valeur
     * @see TypeCase
     *
     * @since 1.0.0
     */
    public void setValeur(TypeCase valeur) {
        this.m_valeur = valeur;
    }

    /**
     * <b>Méthode toString d'une case</b>
     * @return String - valeur de la case
     *
     * La valeur retourner peut être :
     * <ul>
     *     <li>"1" : si la case est occupée par le Joueur 1</li>
     *     <li>"2" : si la case est occupée par le Joueur 2</li>
     *     <li>"" : si la case n'est pas occupée</li>
     * </ul>
     * @see Case#m_valeur
     * @see TypeCase#VIDE
     * @see TypeCase#J1
     * @see TypeCase#J2
     * @since 1.0.0
     */
    @Override
    public String toString() {
        switch (this.m_valeur) {
            case J1:
                return "1";
            case J2:
                return "2";
            default:
                return " ";
        }
    }
}
