package com.antoinethys.puissance4.serveur.moteur;

/**
 * <b>ENUM pour le type de Case</b>
 *
 * <p>ette ENUM défini qui occupe une case.</p>
 * @see TypeCase#VIDE
 * @see TypeCase#J1
 * @see TypeCase#J2
 *
 * @author Antoine Thys
 * @version 1.0.0
 */
public enum TypeCase {
	/**
	 * La case n'est pas occupée
	 */
	VIDE,
	/**
	 * La case est occupée par le Joueur 1
	 */
	J1,
	/**
	 * La case est occupée par le Joueur 2
	 */
	J2
}
