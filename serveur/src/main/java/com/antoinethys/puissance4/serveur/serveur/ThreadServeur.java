package com.antoinethys.puissance4.serveur.serveur;

import com.antoinethys.puissance4.serveur.Json.ThreadJson;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.antoinethys.puissance4.serveur.moteur.Plateau;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * <b>Thread du serveur du jeu</b>
 *
 * @author Antoine Thys
 * @version 1.2.0
 * @see Plateau
 * @see Serveur
 * @see Runnable
 */
public class ThreadServeur implements Runnable {
    /**
     * Serveur lançant le Thread
     * @see Serveur
     * @since 1.0.0
     */
    private Serveur serveur;
    /**
     * Sortie vers les clients
     * @see PrintWriter
     * @since 1.0.0
     */
    private PrintWriter out;
    /**
     * Entrée depuis les clients
     * @see BufferedReader
     * @since 1.0.0
     */
    private BufferedReader in;
    /**
     * Socket utilisé par le serveur
     * @see Socket
     * @see Serveur
     * @since 1.0.0
     */
    private Socket s;
    /**
     * Attribut du joueur utilisant le Thread
     * @since 1.0.0
     */
    private int qui;
    /**
     * Boolean pour stopper la boucle infinie
     * @see ThreadServeur#run()
     * @since 1.0.0
     */
    private boolean marche = true;

    /**
     * <b>Constructeur du Thread</b>
     * <p>
     *     Permet l'instanciation des flux I/O ainsi que
     *     la définition du joueur, du Serveur et du Socket
     * </p>
     * @param serveur Serveur ayant lancé le Thread
     * @param s Socket
     * @see Serveur
     * @see Socket
     * @since 1.0.0
     */
    public ThreadServeur(Serveur serveur, Socket s) {
        this.qui = serveur.incNb();
        this.serveur = serveur;
        this.s = s;
        try {
            out = new PrintWriter(s.getOutputStream(), true);
            in = new BufferedReader(
                    new InputStreamReader(s.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <b>Implémentation venant de Runnable</b>
     *
     * <p>Cette méthode contient toute la logique de l'envoie et la réception des données en JSON</p>
     *
     * <i>Mise à jour de l'arrêt en version 1.1.0</i>
     * <i>Mise à jour de la construction du message en version 1.2.0</i>
     * @see Thread
     * @since 1.0.0
     * @since 1.1.0
     * @since 1.2.0
     */
    @Override
    public void run() {
        try {
            System.out.println("Un client est connecté");
            while (isMarche()) {
                if (serveur.getNb() == 2) {
                    if (serveur.getP().gagne() == 0) {
                        /**
                         * <b>Affichage de la partie de la partie en cours</b>
                         *
                         * Utilisation de Jackson pour la construction des messages du Serveur
                         *
                         * @since 1.2.0
                         * @see ThreadJson#ThreadJson(Integer, Integer, Plateau)
                         * @see ObjectMapper
                         * @see ObjectMapper#writeValueAsString(Object)
                         */
                        ObjectMapper mapper = new ObjectMapper();
                        out.println(mapper.writeValueAsString(new ThreadJson(
                                qui, serveur.getP().getQuiJoue(), serveur.getP(),
                                qui == serveur.getP().getQuiJoue() ? "C'est à  vous de jouer" : "Attendez vous tour")));
                    } else {
                        System.out.println("Joueur " + serveur.getP().gagne() + " a gagné !");
                        /**
                         * <p>
                         *     Lors qu'un joueur gagne la boucle n'est plus brisée
                         *     depuis la version 1.1.0, C'est le serveur qui passe la variable à "false"
                         * </p>
                         * @see ThreadServeur#stop()
                         * @since 1.1.0
                         */
                        continue;
                    }
                    if (qui == serveur.getP().getQuiJoue()) {
                        /**
                         * Le numéro de colonne de l'entrée est convertie en int pour permettre de jouer
                         * @see Integer#parseInt(String)
                         * @see BufferedReader#readLine()
                         * @since 1.0.0
                         */
                        int c = Integer.parseInt(in.readLine());

                        /**
                         * Le Thread passe par le serveur pour jouer un coup sur le plateau
                         * @see Serveur#getP()
                         * @see main.java.moteur.Plateau#joue(int)
                         * @since 1.0.0
                         */
                        serveur.getP().joue(c);

                        /**
                         * On change ensuite de joueur
                         * @see Serveur#getP()
                         * @see Plateau#changeJoueur()
                         * @since 1.0.0
                         */
                        serveur.getP().changeJoueur();
                    } else {
                        Thread.sleep(2000);
                    }
                } else {
                    /**
                     * <b>Attente d'un autre Joueur</b>
                     *
                     * Utilisation de Jackson pour la construction des messages du Serveur
                     *
                     * @since 1.2.0
                     * @see ThreadJson#ThreadJson(Integer)
                     * @see ObjectMapper
                     * @see ObjectMapper#writeValueAsString(Object)
                     */
                    ObjectMapper attente = new ObjectMapper();
                    out.println(attente.writeValueAsString(
                            new ThreadJson(qui,null,null,"Joueur " + qui + " : En attente de joueur...")
                    ));
                    Thread.sleep(2000);
                }
            }
            /**
             * <b>Affichage du gagnant</b>
             *
             * Affichage du gagnant ainsi que du plateau
             *
             * @since 1.2.0
             * @see ThreadJson#ThreadJson(Plateau, Integer)
             * @see ObjectMapper
             * @see ObjectMapper#writeValueAsString(Object)
             */
            ObjectMapper resultat = new ObjectMapper();
            out.println(resultat.writeValueAsString(
                    new ThreadJson(null,null,null,"Le Joueur " + serveur.getP().gagne() + " a gagné !")
            ));
            in.close();
            out.close();
            s.close();
        } catch (Exception e) {
            System.err.println(getClass().getSimpleName()+" : "+e.getMessage());
        }
    }

    /**
     * <b>Méthode permettant de stoper la boucle infinie de jeu</b>
     * @see ThreadServeur#run()
     */
    public void stop() {
        this.setMarche(false);
    }

    /**
     * Vérification de l'attribut marche
     * @return (boolean) - Ã©tat de l'attribut
     */
    public boolean isMarche() {
        return marche;
    }

    /**
     * Setter de l'attribut marche
     * @param marche (boolean) : valeur Ã  donner Ã  marche
     * @see ThreadServeur#marche
     */
    public void setMarche(boolean marche) {
        this.marche = marche;
    }
}
