package com.antoinethys.puissance4.serveur.serveur;

import com.antoinethys.puissance4.serveur.moteur.Plateau;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * <b>Serveur du jeu</b>
 *
 * @author Antoine Thys
 * @version 1.1.0
 * @see Plateau
 * @see ThreadServeur
 */
public class Serveur {
    /**
     * Initialisation du Plateau
     * @see Plateau
     * @since 1.0.0
     */
    private Plateau p = new Plateau();
    /**
     * Tableau de ThreadServeur
     * @see ThreadServeur
     * @since 1.0.0
     */
    private ArrayList<ThreadServeur> ths = new ArrayList<>(2);
    /**
     * Nombre de Threads lancés
     * @since 1.0.0
     */
    private int nb = 0;

    /**
     * <b>Constructeur de la Class</b>
     *
     * <p>Le constructeur permet de lancer les 2 Threads nécessaires pour le fonctionnement du jeu.
     * Le constructeur instancie également le ServerSocket pour la connexion</p>
     *
     * <i>Change de la fonction d'arrêt en version 1.1.0</i>
     * @see ThreadServeur
     * @see ThreadServeur#ThreadServeur(Serveur, Socket)
     * @see ThreadServeur#stop()
     * @see Thread#Thread(Runnable)
     * @see Thread#start()
     * @see Thread#sleep(long)
     * @see ServerSocket#ServerSocket(int)
     * @see ServerSocket#accept()
     * @see Socket
     */
    public Serveur() {
        try {
            ServerSocket ss = new ServerSocket(1234);
            while (true) {
                if (nb < 2) {
                    System.out.println("Serveur à l'écoute");
                    Socket s = ss.accept();
                    ths.add(new ThreadServeur(this, s));
                    new Thread(ths.get(nb-1)).start();
                }
                if (p.gagne() != 0) {
                    ths.forEach(ThreadServeur::stop);
                    ss.close();
                    break;
                }
            }
        } catch (Exception exc) {
            System.err.println(getClass().getSimpleName()+" : "+exc.getMessage());
        }
    }

//    /**
//     * Méthode de test de la Class
//     * @param args non utilisé
//     */
//    public static void main(String[] args) {
//        new Serveur();
//    }

    /**
     * <b>Getter du Plateau</b>
     * <p>Permet de récupérer le plateau de jeu</p>
     * @return (Plateau) - plateau stocké en attribut
     */
    public Plateau getP() {
        return p;
    }

    /**
     * <b>Getter de l'attribut nb</b>
     * @return (int) - attribut nb
     */
    public int getNb() {
        return nb;
    }

    /**
     * <b>Incrémentation de l'attribut nb</b>
     * @return (int) - attribut nb incrémenté
     */
    public int incNb() {
        nb++;
        return nb;
    }
}
