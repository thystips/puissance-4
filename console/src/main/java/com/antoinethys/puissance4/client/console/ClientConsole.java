package com.antoinethys.puissance4.client.console;

import java.io.*;
import java.net.Socket;

/**
 * <b>Classe pour le Client du jeu en Console</b>
 * @author Antoine Thys
 * @version 1.0.0
 * @see ClientConsole
 * //@see Serveur
 */
public class ClientConsole {
    private Socket s;
    private PrintWriter out;
    private BufferedReader in;

    /**
     * <b>Constructeur de la classe</b>
     * @see ClientConsole
     * @see Socket
     * Le constructeur permet l'instanciation :
     * <ul>
     *     <li>du Socket</li>
     *     <li>de la sortie vers le serveur</li>
     *     <li>de l'entrée depuis le serveur</li>
     * </ul>
     * @see Socket#Socket(String, int)
     * @see PrintWriter#PrintWriter(OutputStream, boolean)
     * @see BufferedReader#BufferedReader(Reader)
     * @since 1.0.0
     */
    public ClientConsole() {
        try {
            s = new Socket("localhost", 1234);
            out = new PrintWriter(s.getOutputStream(), true);
            in = new BufferedReader(
                    new InputStreamReader(s.getInputStream()));
        } catch (IOException exc) {
            System.err.println(exc.getMessage());
        }
    }

    /**
     * Cette méthode permet de lire les données venant du serveur
     * @throws IOException throws du reader
     * @return String du Json venant du Serveur
     * @see BufferedReader#readLine()
     * @since 1.0.0
     * <i>Depuis la version 1.2.0 cette méthode retourne une String à la place d'un JSONObject</i>
     * @since 1.2.0
     */
    public String lisServeur() throws IOException {
        return in.readLine();
    }

    /**
     * Cette méthode permet d'envoyer la colonne à jouer au serveur
     * @param c colonne à jouer
     * @since 1.0.0
     */
    public void envoieColonneAuServeur(int c) {
        out.println(c);
    }

    /**
     * Méthode permettant de "dire poliment au revoir"
     *
     * @throws IOException Exception du socket
     * @throws InterruptedException Exception du Thread
     * @see Thread#sleep(long)
     * @see Socket#close()
     * @see PrintWriter#close()
     * @see BufferedReader#close()
     * @since 1.0.0
     */
    public void auRevoir() throws IOException, InterruptedException {
        Thread.sleep(2000);
        System.out.println("Au revoir");
        in.close();
        out.close();
        s.close();
    }
}
