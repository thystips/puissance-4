package com.antoinethys.puissance4.client;

import com.antoinethys.puissance4.client.console.ClientConsole;
import com.antoinethys.puissance4.serveur.Json.ThreadJson;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Scanner;

/**
 * Class de lancement du jeu
 * @author Antoine Thys
 * @version 1.0.0
 * @since 1.3.0
 */
public class Launcher {
    /**
     * <b>Cette Méthode permet de lancer le jeu sur le Client</b>
     * Elle contient :
     * <ul>
     *    <li>La gestion du clavier pour envoyer la colonne jouée au serveur</li>
     *    <li>L'affichage des messages envoyés par le serveur</li>
     *    <li>L'affichage du plateau</li>
     *    <li>Fermeture de la connexion</li>
     * </ul>
     * @param args non utilisé
     * @since 1.0.0
     * //@see Serveur
     * <i>Depuis la version 1.2.0 la reponse du serveur est récupérée en String afin d'être traitée par Jackson et d'être convertie en Object ThreadJson</i>
     */
    public static void main(String[] args) {
        ClientConsole client = new ClientConsole();
        Scanner clav = new Scanner(System.in);
        ObjectMapper mapper = new ObjectMapper();
        try {
            while (true) {
                ThreadJson reponse = mapper.readValue(client.lisServeur(), ThreadJson.class);
                String message = reponse.getMessage();
                if (message.contains("attente")) {
                    System.out.println(message);
                } else if (message.contains("gagn")) {
                    System.out.println(message);
                    break;
                } else {
                    assert reponse.getPlateau() != null;
                    String stringPlateau = reponse.getPlateau().toString();
                    for (String l : stringPlateau.split("\\n")) System.out.println(l);
                    System.out.println(message);
                    if (message.contains("vous de jouer")) {
                        System.out.println("Entrez la colonne que vous souhaitez jouer : ");
                        client.envoieColonneAuServeur(clav.nextInt());
                    }
                }
            }
            clav.close();
            client.auRevoir();
        } catch (IOException | InterruptedException exc) {
            System.err.println(exc.getMessage());
        }
    }
}
