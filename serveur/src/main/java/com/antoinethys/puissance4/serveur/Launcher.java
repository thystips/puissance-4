package com.antoinethys.puissance4.serveur;

import com.antoinethys.puissance4.serveur.serveur.Serveur;

/**
 * <b>Lanceur pour le serveur du jeu</b>
 *
 * @author Antoine Thys
 * @version 1.0.0
 * @since 1.3.0
 * @see Serveur
 */
public class Launcher {
    public static void main(String[] args) {
        new Serveur();
    }
}
