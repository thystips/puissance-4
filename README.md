# Puissance 4 en réseau dans un terminal

[![pipeline status](https://gitlab.com/antoine33520/puissance-4/badges/master/pipeline.svg)](https://gitlab.com/antoine33520/puissance-4/-/commits/master)

## Description

**Projet de cours :** Réalisation d'un **puissance 4** en réseau communiquant en **JSON** et fonctionnant dans un terminal.

---

## Instructions

### Version 1.2.0

Pour faire fonctionner ce programme en version `1.2.0` 3 étapes sont nécessaires :

* Lancer la méthode `main` contenue dans la classe `Serveur` pour le `serveur`.
* Lancer la méthode `main` contenue dans la classe `ClientConsole` pour le `Joueur1`.
* Lancer la méthode `main` contenue dans la classe `ClientConsole` pour le `Joueur2`.

Le jeu se déroulera ensuite avec les consoles du `Joueur1` et `Joueur2`.

---

## Ressources

**Documentation** : <http://antoine33520.gitlab.io/puissance4>