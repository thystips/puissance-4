package com.antoinethys.puissance4.serveur.moteur;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Scanner;

/**
 * <b>Class définissant le plateau de jeu</b>
 * @author Antoine Thys
 * @version 1.0.0
 * @see Case
 * @see Plateau
 */
public class Plateau {
    @JsonProperty("nb")
    private static final int nb = 7;
    @JsonProperty("tab")
    private Case[][] m_tab = new Case[nb][nb];
    @JsonProperty("joueur")
    private int quiJoue = 1;

    /**
     * <b>Constructeur de la Class</b>
     *
     * <p>Le ocnstructeur permet d'instancer le plateau de jeu
     * grâce à deux dimensions contenant des cases vides.</p>
     * @see Case
     * @see Case#Case()
     * @see TypeCase
     * @see TypeCase#VIDE
     * @since 1.0.0
     */
    public Plateau() {
        for (int l = 0; l < nb; l++) {
            for (int c = 0; c < nb; c++) {
                this.m_tab[l][c] = new Case();
            }
        }
    }

//    /**
//     * main permettant de tester la classe
//     * @param args non utilisé
//     * @since 1.0.0
//     */
//    public static void main(String[] args) {
//        Scanner clav = new Scanner(System.in);
//        Plateau p = new Plateau();
//        while (p.gagne() == 0) {
//            System.out.println(p);
//            int c = -1;
//            while (c < 0 || c >= nb) {
//                System.out.print("Joueur " + p.getQuiJoue() + ", quelle colonne ? ");
//                c = clav.nextInt();
//            }
//            p.joue(c);
//            p.changeJoueur();
//        }
//        System.out.println(p);
//        System.out.println("J" + p.gagne() + " a gagné !");
//        clav.close();
//    }

    /**
     * <b>Méthode pour jouer un coup</b>
     *
     * <p>Cette méthode ajoute un pion sur la première case VIDE de la colonne choisie
     * Cette case devient donc occupée par J1 ou J2</p>
     * @param colonne colonne où ajouter le pion
     *
     * @see Plateau#m_tab
     * @see TypeCase
     * @see Case#getValeur()
     * @see Case#setValeur(TypeCase)
     * @since 1.0.0
     */
    public void joue(int colonne) {
        int l = 0;
        while (this.m_tab[l][colonne].getValeur() != TypeCase.VIDE) {
            l++;
        }
        this.m_tab[l][colonne].setValeur(this.quiJoue == 1 ? TypeCase.J1 : TypeCase.J2);
    }

    /**
     * <b>Permet de définir si un joueur a gagné</b>
     *
     * <p>
     *     Cette méthode vérifie avec des boucles imbriquées.
     *     Chaque schéma est verifié et s'il y a une correspondance
     *     la méthode renvoit la valeur du dernier pion posé sinon
     *     le retour est 0.
     * </p>
     * @return (int) 0 ou numéro du Joueur gagnant
     * @see Plateau#m_tab
     * @see TypeCase
     * @see Case#getValeur()
     * @since 1.0.0
     */
    public int gagne() {
        for (int l = 0; l < nb; l++) {
            for (int c = 0; c <= nb - 4; c++) {
                if (m_tab[l][c].getValeur() != TypeCase.VIDE &&
                        m_tab[l][c].getValeur() == m_tab[l][c + 1].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l][c + 2].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l][c + 3].getValeur())
                    return m_tab[l][c].getValeur().ordinal();
            }
        }

        for (int l = 0; l <= nb - 4; l++) {
            for (int c = 0; c < nb; c++) {
                if (m_tab[l][c].getValeur() != TypeCase.VIDE &&
                        m_tab[l][c].getValeur() == m_tab[l + 1][c].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l + 2][c].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l + 3][c].getValeur())
                    return m_tab[l][c].getValeur().ordinal();
            }
        }

        for (int l = 0; l <= nb - 4; l++) {
            for (int c = 0; c <= nb - 4; c++) {
                if (m_tab[l][c].getValeur() != TypeCase.VIDE &&
                        m_tab[l][c].getValeur() == m_tab[l + 1][c + 1].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l + 2][c + 2].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l + 3][c + 3].getValeur())
                    return m_tab[l][c].getValeur().ordinal();
            }
        }

        for (int l = 0; l <= nb - 4; l++) {
            for (int c = nb - 4; c < nb; c++) {
                if (m_tab[l][c].getValeur() != TypeCase.VIDE &&
                        m_tab[l][c].getValeur() == m_tab[l + 1][c - 1].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l + 2][c - 2].getValeur() &&
                        m_tab[l][c].getValeur() == m_tab[l + 3][c - 3].getValeur())
                    return m_tab[l][c].getValeur().ordinal();
            }
        }

        return 0;
    }

    /**
     * <b>Méthode toString</b>
     *
     * <p>Permet l'affichage du plateau de jeu et des pions posés</p>
     * @see Plateau#m_tab
     * @return (String) : Plateau de jeu
     * @since 1.0.0
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (int l = nb - 1; l >= 0; l--) {
            StringBuilder ligne = new StringBuilder();
            for (int c = 0; c < nb; c++) {
                ligne.append(this.m_tab[l][c]);
            }
            res.append(ligne).append("\n");
        }
        res.append("-------\n0123456");
        return res.toString();
    }

    /**
     * <b>Change le joueur qui doit jouer</b>
     *
     * <p>C'est l'équivalent d'un setter pour la variable quiJoue</p>
     * @see Plateau#quiJoue
     * @since 1.0.0
     */
    public void changeJoueur() {
        this.quiJoue = this.quiJoue == 1 ? 2 : 1;
    }

    /**
     * <b>Getter de m_tab</b>
     * @return m_tab
     * @since 1.0.0
     */
    public Case[][] getTab() {
        return m_tab;
    }

    /**
     * <b>Permet de récupérer le joueur qui doit jouer son tour</b>
     * @return joueur en cours
     * @since 1.0.0
     */
    public int getQuiJoue() {
        return quiJoue;
    }

}
